########
### This script takes the output of flash_align
### as an input process fastq, merge and correct errors
### output is ready for post-analysis
########


local chain="IGH"
local species="mouse"
local details="cdr1nt,cdr1aa,cdr2nt,cdr2aa,contignt"
local readnum="500000"
local qualitycutoff="3"


migdir="02_migmap"
mkdir "${migdir}"
migoutdir="${unique}/${migdir}"

migpath="/home/antibody/migmap-1.0.3/"

# --- execution
echo '. . .  Processing file'
java -jar "${migpath}"/migmap-1.0.3.jar -R "${chain}" -S "${species}" -n "${readnum}" -q "${qualitycutoff}" \
	--allow-noncanonical --allow-incomplete --details "${details}" "${flashdir}"/* \
	"${migoutdir}"/"${unique}".migmap.out.txt &>> "${migoutdir}"/"${unique}".migmap.log


# --- merge contigs - merges clonotypes
echo '. . . Merging contigs'
java -cp "${migpath}"/migmap-1.0.3.jar com.antigenomics.migmap.MergeContigs \
	"${migdir}"/"${unique}".migmap.out.txt "$migdir"/"${unique}".migmap.merged.out.txt


# --- error correction - filter erroneous sub-variants
echo '. . . Processing error correction'
java -cp "${migpath}"/migmap-1.0.3.jar com.antigenomics.migmap.Correct \
	"${migdir}"/"${unique}".migmap.out.txt "${migdir}"/"${unique}".corrected.txt


# --- post-analysis - summarize hypermutations and generate clonotype trees run
echo '. . . Summarizing hypermutations'
java -cp "${migpath}"/migmap-1.0.3.jar com.antigenomics.migmap.Analyze \
	-S "${species}" -R ${chain} "${migdir}"/"${unique}".migmap.out.txt "${migdir}"/"${unique}".migmap.analyze.out


### --allow-noncanonical:
##### report clonotypes that have non-canonical CDR3
##### non-canonical = does not start with C or end with F/W residue

### --allow-incomplete:
##### report clonotypes with partial CDR3 mapping
##### partial = lacking J germline part, etc
##### default: no reported

### --details:
##### specifies the nucleotide and amino acid sequences for certain FR/CDR regions
##### that will be added to the output