
#!/bin/sh

#########
#### This function takes in a paired-end fastq sequence
#### as an input and merges short reads into unpaired longer
#### reads
#########

r1="${1}"
r2="${2}"


	# | sed -e 's/^\(ls \)//g')
# r2=$(echp $r2 | sed -e 's/^\(ls \)//g')

unique=$(echo "$r{1}" | sed -e 's/\(_S[0-9][0-9]_L001_R[1-2]_001.fastq.gz\)$*//g')
outdir="01_flash"
logfile="${unique}.flash.log"
flashdir="${unique}/${outdir}"

mkdir -p "${flashdir}"

cd "${unique}"

flash "${r1}" "${r2}" -M 350 -o "${unique}" -d "${flashdir}" 2>&1 | tee "${logfile}"







